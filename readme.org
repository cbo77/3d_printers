#+TITLE: 3D printing guide

* Introduction
* Modelling

  Models are meshes which represent the `thing' you want to
  print. Modelling is done, usually, in CAD software such as

    - [[https://www.freecadweb.org/][FreeCAD]]
    - [[http://www.openscad.org/][OpenSCAD]]

  In addition, 3D drawing software such as [[https://www.blender.org/][Blender]] can be used.

* Slicing

  The model needs to be cut up into layers that the printer can print;
  this process is called /slicing/. This requires another bit of
  software, a /slicer/. Commonly used programs:

    - Slic3r
    - Cura

* Printing
** Bed Adhesion
   
   Print not sticking? Build up of dirt/oil on print bed causes bed to
   become non-adhering to PLA/whatever

   To help:

     - Re-calibrate the printer

       The print bed can move over time (especially with repeated
       heating and cooling). Re-calibration should help.

     - Clean the print surface

       1. Remove any residual filament from the surface

       2. Clean with methylated spirits/IPA/whatever

       3. Add glue (PVA, glue stick) and wipe off with IPA soaked towel
          the goal being to gain a clean even surface of glue [[https://www.youtube.com/watch?v=ShFaJ027pFs][youtube]]

       4. Remember to clean with IPA before printing, and acetone
          occasionally [[https://www.youtube.com/watch?v=GE-lrRbU124][prusa beginner's guide]]

     - Clean the nozzle

       - [[https://www.youtube.com/watch?v=g8uvh6kvr54][Tom Sanlanderer's nozzle cleaning guide]]

       - Over time the nozzle can become clogged (especially when
         moving from one material to another) - prevents the filament
         from properly extruding

       - Common cleaning methods are the `hot pull' and the 'cold
         pull'
         - Hot pull: [[https://www.youtube.com/watch?v=6_TFCiLnQ94][youtube]]
           
           1. Heat up to melt temperature for the material in the
              nozzle (e.g. 210-220 deg C for PLA)

           2. Disengage the extruder (open the compartment)

           3. Pull the filament out from the nozzle/hotend

           4. There should be a plug on the end of the filament in the
              shape (roughly) of the inside of the nozzle

           5. Repeat from the beginning 5 or so times to ensure all
              the dirt is removed
           
         - Cold pull: [[https://www.youtube.com/watch?v=hBkTeXxcFi8][youtube]] [[http://bukobot.com/nozzle-cleaning][bukobot guide]]
           
           As with hot pull, but at lower temperature

           1. Heat up to melt temperature for the material in the
              nozzle (e.g. 210-220 deg C for PLA)

           2. Disengage the extruder (open the compartment)

           3. Turn off heating, allow hot end to cool to around 90C

           4. Pull the filament out from the nozzle/hotend

           5. There should be a plug on the end of the filament in the
              shape (roughly) of the inside of the nozzle

           6. Repeat from the beginning 5 or so times to ensure all
              the dirt is removed
           

         
